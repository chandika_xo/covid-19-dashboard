import axios from "axios";

const url="https://hpb.health.gov.lk/api/get-current-statistical";

export const fetchData=async()=>{
    try{
        const {data}=await axios.get(url);

        const modifiedData={


            update_date_time:data.data.update_date_time,
            
            global_new_cases:data.data.global_new_cases,
            global_total_cases:data.data.global_total_cases,
            global_death:data.data.global_deaths,
            global_recovered:data.data.global_recovered,
            local_new_cases:data.data.local_new_cases,
            local_total_cases:data.data.local_total_cases,
            local_total_number_of_individuals_in_hospitals:data.data.local_total_number_of_individuals_in_hospitals,
            local_deaths:data.data.local_deaths,
            local_recovered:data.data.local_recovered,
            // hospital_data:data.data.hospital_data,
             daily_pcr_testing_data:data.data.daily_pcr_testing_data,
        }
      

        return modifiedData;

    }
    catch(error){
        
    }
}

// chart api

