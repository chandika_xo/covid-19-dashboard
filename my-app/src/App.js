//import logo from './logo.svg';
//import './App.css';

import React from "react";
import {fetchData} from "./api";
import styles from "./App.module.css";
import {Cards,Graph,CountryPicker} from "./components";
import { render } from 'react-dom';

class App extends React.Component {
state={
  data:{},
}


  async componentDidMount(){
    const fetchedData=await fetchData();
    this.setState({data:fetchedData});
  }

  render(){
    const { data } = this.state;

  return (
    <div className={styles.container}>
      <style>{'body { background-color: #F1F1F1}'}</style>
      <Cards data={data}/>
    
    </div>
  );
}
}

export default App;
