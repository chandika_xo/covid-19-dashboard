import React from "react"
import {Card,CardContent,Typography} from "@material-ui/core";
import styles from "./cards.module.css";
import CountUp from "react-countup";
import cx from "classnames";
import Container from '@material-ui/core/Container';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';

import FindInPageIcon from '@material-ui/icons/FindInPage';
import LocalHospitalIcon from '@material-ui/icons/LocalHospital';
import TimelineIcon from '@material-ui/icons/Timeline';
import RestoreIcon from '@material-ui/icons/Restore';
import PersonAddIcon from '@material-ui/icons/PersonAdd';


import Piechart from '../Chart/Piechart';
import Graph from '../Chart/Graph';
import NavBar from '../NavBar';
import Chart from "react-apexcharts";
import Toolbar from '@material-ui/core/Toolbar';
import { jsx, css } from "@emotion/core";
            
    const Cards=({ data:{global_new_cases,global_total_cases,
        global_death,global_recovered,local_new_cases,
        local_total_cases,local_total_number_of_individuals_in_hospitals,local_deaths,
        local_recovered,update_date_time,daily_pcr_testing_data} })=>{

        console.log(daily_pcr_testing_data);
        

        if(!global_recovered){
            return "loader";
        }
    


       


// ////////////////////////////////////////////////////////////////////////////////////////

const output = daily_pcr_testing_data.map(({count, ...rest}) => ({...rest, count: +count}));
var groupedByWeek = output.reduce((m, o) => {
    var monday = getMonday(new Date(o.date));
    var mondayYMD = monday.toISOString().slice(0,10);
    var found = m.find(e => e.date === mondayYMD);
    if (found) {
    found.count += parseInt(o.count);
    } else {
        o.date = mondayYMD;
        m.push(o);
    }
    return m;
}, []);

console.log(groupedByWeek);
const X = groupedByWeek.map(a => a.date);
const Y = groupedByWeek.map(a => a.count);
console.log(X);



function getMonday(d) {
    var day = d.getDay();
    var diff = d.getDate() - day + (day === 0 ? -6 : 1);  
    return new Date(d.setDate(diff));
}


const state = {

    options: {
      chart: {
        id: "basic-bar"
      },
      xaxis: {
        categories: X
      }
    },
    series: [
      {
        name: "series-1",
        data: Y
      }
    ]
  };
// ///////////////////////////////////////////////////////////////////////////////


      
    return(
        <div>
             {/* <div className="mixed-chart">
            <Chart
              options={state.categories}
              series={state.series}
              type="bar"
              width="500"
            />
          </div> */}
            <NavBar/>
            <Container>

                <Card className={cx(styles.maincard)}>
                       
                <Typography component={'span'} variant="h6"   gutterBottom>
                     <Box  className={cx(styles.maincard2)}  m={1}>
                          Global Cases
                         </Box>
                     </Typography>

                   

                <Grid container spacing={3} justify="center">

                <Grid item >
            <Paper>

            <Grid item component={Card} xs={12} className={cx(styles.Card,styles.newCase)}>
                 <CardContent>
               
                
                      
                     <Typography component={'span'} variant="h6"  color="textSecondary" gutterBottom>
                     <Box  fontWeight="fontWeightBold" m={1}>
                          New Cases
                         </Box>
                     </Typography>
                    
                     <PersonAddIcon color="disabled" fontSize="large"/>
                     <Typography component={'span'} className={cx(styles.count_local_recovered)}>
                     <Box  fontWeight="fontWeightBold" fontSize={26} m={1}>
                      
                     
                     <CountUp
                        fontSize={30}
                         start={0}
                         end= {global_new_cases}
                         duration={1.5}
                         separator=","
                         />
                         </Box>
                         </Typography>

                  
                 </CardContent>
             </Grid>
             </Paper>
             </Grid>
          {/* ////////////////////////////////////////////////////////////////////////////////////////////// */}

    

      <Grid item >
        <Paper>

            <Grid item component={Card} xs={12} className={cx(styles.Card,styles.TotalCases)}>
                 <CardContent>
               
                     <Typography component={'span'} variant="h6" color="textSecondary" gutterBottom>
                     <Box   fontWeight="fontWeightBold" m={1}>
                         Total Cases
                     </Box>
                     </Typography>
                     
                     <FindInPageIcon color="disabled" fontSize="large"/>
                     <Typography component={'span'} className={cx(styles.count_local_recovered1)}>
                     <Box  fontWeight="fontWeightBold" fontSize={26} m={1}>
                      
                     
                     <CountUp
                        fontSize={50}
                         start={0}
                         end= {global_total_cases}
                         duration={1.5}
                         separator=","
                         />
                         </Box>
                         </Typography>

                 </CardContent>
             </Grid>
             </Paper>
      </Grid>

        {/* ////////////////////////////////////////////////////////////////////////////////////////////// */}

      <Grid item >
        <Paper>

            <Grid item component={Card} xs={12} className={cx(styles.Card,styles.Deaths)}>
                 <CardContent>
               
                     <Typography component={'span'} variant="h6"  color="textSecondary" gutterBottom>
                     <Box  fontWeight="fontWeightBold" m={1}>
                         Total Deaths
                         </Box>
                         </Typography>
                     
                     <TimelineIcon color="disabled" fontSize="large"/>
                     <Typography component={'span'} className={cx(styles.count_local_recovered2)}>
                     <Box fontWeight="fontWeightBold" fontSize={26} m={1}>
                      
                     
                     <CountUp
                        fontSize={50}
                         start={0}
                         end= {global_death}
                         duration={1.5}
                         separator=","
                         />
                         </Box>
                         </Typography>

                    
                 </CardContent>
             </Grid>
             </Paper>
      </Grid>

            {/* ////////////////////////////////////////////////////////////////////////////////////////////// */}

      <Grid item >
        <Paper>

            <Grid hight='300'  item component={Card} xs={12} className={cx(styles.Card,styles.Recovered)}>
                 <CardContent>
               
                     <Typography component={'span'} variant="h6" color="textSecondary" gutterBottom>
                     <Box  fontWeight="fontWeightBold" m={1}>
                         Recovered
                         </Box>
                         </Typography>
                     
                     <RestoreIcon color="disabled" fontSize="large"/>
                     <Typography component={'span'}  className={cx(styles.count_local_recovered3)}>
                     <Box fontWeight="fontWeightBold" fontSize={26} m={1}>
                      
                     
                     <CountUp
                        fontSize={50}
                         start={0}
                         end= {global_recovered}
                         duration={1.5}
                         separator=","
                         />
                         </Box>
                         </Typography>

                 </CardContent>
             </Grid>
             </Paper>
      </Grid>
      </Grid>
      <Typography component={'span'} color="textSecondary" >
                         
                   <Box textAlign="right"  fontSize={13} className={cx(styles.update)}>
                      Last updated- {update_date_time}
                   </Box>

                   
                   </Typography>
                   
      </Card>
    
      {/* #####################################################################################################*/}
     
      
      <Grid  justify="center">

<Grid item xs={6}>
<Grid item xs={12} className={styles.Card1}>
         
<Graph/>


</Grid>
</Grid>
</Grid>
 {/* ##################################################################################################### */}
     
     
 <Card className={cx(styles.maincard)} >
                       
                       <Typography component={'span'} variant="h6"   gutterBottom>
                            <Box  className={cx(styles.maincard2)} m={1}>
                                 Local Cases
                                </Box>
                            </Typography>


<Grid container spacing={3} justify="center">
                <Grid item >
            <Paper>

            <Grid item component={Card} xs={12} className={cx(styles.Card,styles.local_new_cases)}>
                 <CardContent>
               
                     <Typography component={'span'} variant="h6"  color="textSecondary" gutterBottom>
                     <Box  fontWeight="fontWeightBold" m={1}>
                          New Cases
                         </Box>
                         </Typography>
                    
                     <PersonAddIcon  color="disabled" fontSize="large"/>
                     <Typography component={'span'} className={cx(styles.count_local_recovered)}>
                     <Box fontWeight="fontWeightBold" fontSize={26} m={1}>
                      
                     
                     <CountUp
                        fontSize={50}
                         start={0}
                         end= {local_new_cases}
                         duration={1.5}
                         separator=","
                         />
                         </Box>
                         </Typography>

                 
                 </CardContent>
             </Grid>
             </Paper>
      </Grid>

            {/* ////////////////////////////////////////////////////////////////////////////////////////////// */}

      <Grid item >
        <Paper>

            <Grid item component={Card} xs={12} className={cx(styles.Card,styles.local_total_cases)}>
                 <CardContent>
               
                     <Typography component={'span'} variant="h6"  color="textSecondary" gutterBottom>
                     <Box  fontWeight="fontWeightBold" m={1}>
                         Total Cases
                     </Box>
                     </Typography>

                     <FindInPageIcon color="disabled" fontSize="large"/>
                     <Typography component={'span'}  className={cx(styles.count_local_recovered1)}>
                     <Box  fontWeight="fontWeightBold" fontSize={26} m={1}>
                      
                     
                     <CountUp
                        fontSize={50}
                         start={0}
                         end= {local_total_cases}
                         duration={1.5}
                         separator=","
                         />
                         </Box>
                         </Typography>

                   
                 </CardContent>
             </Grid>
             </Paper>
      </Grid>

            {/* ////////////////////////////////////////////////////////////////////////////////////////////// */}
      <Grid item >
        <Paper>

            <Grid item component={Card} xs={12} className={cx(styles.Card,styles.local_total_cases_in_hospitals)}>
                 <CardContent>
               
                     <Typography component={'span'} variant="h6"  color="textSecondary" gutterBottom>
                     <Box  fontWeight="fontWeightBold" m={1}>
                         Hospitalized
                         </Box>
                         </Typography>
                     
                     <LocalHospitalIcon color="disabled" fontSize="large"/>
                     <Typography component={'span'} className={cx(styles.count_local_recovered4)}>
                     <Box fontWeight="fontWeightBold" fontSize={26} m={1}>
                      
                     
                     <CountUp
                        fontSize={50}
                         start={0}
                         end= {local_total_number_of_individuals_in_hospitals}
                         duration={1.5}
                         separator=","
                         />
                         </Box>
                         </Typography>

                   
                 </CardContent>
             </Grid>
             </Paper>
      </Grid>

            {/* ////////////////////////////////////////////////////////////////////////////////////////////// */}
      <Grid item >
        <Paper>

            <Grid item component={Card} xs={12} className={cx(styles.Card,styles.local_deaths)}>
                 <CardContent>
               
                     <Typography component={'span'} variant="h6"  color="textSecondary" gutterBottom>
                     <Box  fontWeight="fontWeightBold" m={1}>
                         Total Deaths
                         </Box>
                         </Typography>
                     
                     <TimelineIcon color="disabled" fontSize="large"/>
                     <Typography component={'span'}  className={cx(styles.count_local_recovered2)}>
                     <Box fontWeight="fontWeightBold" fontSize={26} m={1}>
                      
                     
                     <CountUp
                        fontSize={50}
                         start={0}
                         end= {local_deaths}
                         duration={1.5}
                         separator=","
                         />
                         </Box>
                         </Typography>

                     
                 </CardContent>
             </Grid>
             </Paper>
      </Grid>
      </Grid>

            {/* ////////////////////////////////////////////////////////////////////////////////////////////// */}
<Grid item>


      <Card className={cx(styles.maincard1)}>
      <Piechart/>
      <Grid item  >
        <Paper>

            <Grid item component={Card} xs={12} className={cx(styles.Card,styles.local_recovered)}>
                 <CardContent>
               
                     <Typography component={'span'}  variant="h6" color="textSecondary" gutterBottom>
                     <Box  fontWeight="fontWeightBold" m={1}>
                         Recovered
                         </Box>
                         </Typography>
                     
                     <RestoreIcon color="disabled" fontSize="large"/>
                     <Typography component={'span'}  className={cx(styles.count_local_recovered3)}>
                     <Box  fontWeight="fontWeightBold" fontSize={26} m={1}>
                      
                     
                     <CountUp
                        fontSize={50}
                         start={0}
                         end= {local_recovered}
                         duration={1.5}
                         separator=","
                         />
                         </Box>
                         </Typography>

                    
                 </CardContent>
             </Grid>
             </Paper>
             </Grid>
      </Card>
  
      
</Grid>


      
      
      {/* ##################################################################################################### */}
     
     
     


 

      <Typography component={'span'} color="textSecondary" >
                         
                         <Box textAlign="right"  fontSize={13} className={cx(styles.update)}>
                            Last updated- {update_date_time}
                         </Box>
      
                         
                         </Typography>
            </Card>
      


    </Container>
      
    </div>

    )
}
export default Cards;