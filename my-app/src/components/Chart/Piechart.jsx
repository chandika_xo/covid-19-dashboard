import React, { Component } from "react";
import Chart from "react-apexcharts";
import cx from "classnames";
import styles from "./Chart.module.css";
import ReactApexChart from "react-apexcharts";


class Piechart extends Component {
  constructor(props) {
    super(props);
    this.state = {
          
        series: [44, 55, 41, 17, 15],
        options: {
            labels: ['local_total_cases', 'local_recovered', 'local_active_cases', 'local_deaths'],
          chart: {
            width: 380,
            type: 'donut',
          },
          plotOptions: {
            pie: {
              startAngle: -90,
              endAngle: 270
            }
          },
          dataLabels: {
            enabled: false
          },
          fill: {
            type: 'gradient',
          },
          legend: {
            formatter: function(val, opts) {
              return val + " - " + opts.w.globals.series[opts.seriesIndex]
            }
          },
          title: {
            text: 'Local Covid Statistic visualization'
          },
          responsive: [{
            breakpoint: 480,
            options: {
              chart: {
                width: 200
              },
              legend: {
                position: 'bottom'
              }
            }
          }]
        },
      
      
      };
    }

  


    componentDidMount() {
        fetch("https://hpb.health.gov.lk/api/get-current-statistical")
          .then((res) => res.json())
          .then((data) => {
            console.log(data);
            if (data !=null) {
              
    
              
              this.setState(prevState => {
                let series = Object.assign({}, prevState.series);  
                let local_total_cases = data.data.local_total_cases;
                let local_recovered = data.data.local_recovered;
                let local_active_cases = data.data.local_active_cases;
                let local_deaths = data.data.local_deaths;
                series = [local_total_cases,local_recovered,local_active_cases,local_deaths];  
                
                return { series };                                
              })
    
    
            }
            
        
           
          })
        
          .catch(console.log);
      }
      
    
     
 

  render() {
    


   
    return (
            

        <div id="chart">
    <ReactApexChart options={this.state.options} series={this.state.series} type="donut" width={400} />
  </div>
    );
    }
  

  
}


export default Piechart;