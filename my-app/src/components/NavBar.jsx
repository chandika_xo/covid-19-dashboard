import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import MoreIcon from '@material-ui/icons/MoreVert';
import Box from '@material-ui/core/Box';
import covidlogo from './covidlogo.png';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    marginBottom:55,
   
  },
  header: {
    backgroundColor: "#fff",
    color: "black",
    boxShadow: "0px 0px 0px 0px",
    alignContent:'center',
    fontFamily: 'Helvetica',
    textAnchor: 'start',
    
    fontSize: 18,
    fontWeight: 900,


  },
 
  toolbar: {
    height:40,
  
    
    backgroundColor:'inherit',
   
  },
  title: {
    flexGrow: 1,
  
    fill: 'black',
  }
}));

export default function NavBar() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <AppBar className={classes.header} position="static">
        <Toolbar className={classes.toolbar}>

      <Box>
      <img src={covidlogo}   width="200" height="100"/>
      </Box>
      
          <Typography component={'span'} className={classes.title} variant="h4" noWrap>
            
          <Box fontWeight="fontWeightBold" textAlign="center" m={1}>
            COVID STATISTICS 
            </Box>
          </Typography>
          
       
        </Toolbar>
      </AppBar>
    </div>
  );
}